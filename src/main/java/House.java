import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {

    private String number;
    private String address;
    private Person housekeeper;
    private List<Flat> flats;

    public House(String number, String address, Person housekeeper, List<Flat> flats) {
        this.number = number;
        this.address = address;
        this.housekeeper = housekeeper;
        this.flats = flats;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Person getHousekeeper() {
        return housekeeper;
    }

    public void setHousekeeper(Person housekeeper) {
        this.housekeeper = housekeeper;
    }

    public List<Flat> getFlats() {
        return flats;
    }

    public void setFlats(List<Flat> flats) {
        this.flats = flats;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        House house = (House) o;
        return number.equals(house.number) &&
                address.equals(house.address) &&
                housekeeper.equals(house.housekeeper) &&
                Objects.equals(flats, house.flats);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, address, housekeeper, flats);
    }
}
