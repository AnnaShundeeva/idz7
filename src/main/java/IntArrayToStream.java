import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Scanner;

//Надо ли закрывать потоки?

public class IntArrayToStream {

    /*1. Записать массив целых чисел в двоичный поток. Прочитать массив целых чисел из
    двоичного потока. Предполагается, что до чтения массив уже создан, нужно прочитать n
    чисел, где n — длина массива.*/

    public static void writeBytes(int[] array) {
        File file = new File( "file.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("Неверный путь к файлу");
        }
        try (DataOutputStream out = new DataOutputStream(new FileOutputStream(file))) {
            for (int num : array) {
                try {
                    out.writeInt(num);
                } catch (IOException e) {
                    System.out.println("Ошибка при записи");
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
    }

    public static int[] readBytes(int n) {
        File file = new File("file.txt");
        try (DataInputStream in = new DataInputStream(new FileInputStream(file))) {
            int[] array = new int[n];
            for (int i = 0; i < n; i++) {
                array[i] = in.readInt();
            }
            return array;
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return new int[0];
    }

//2. Аналогично, используя символьные потоки. В потоке числа должны разделяться пробелами

    public static void writeToSymbol(int[] array) {
        File file = new File("file.txt");
        try {
            file.createNewFile();
        } catch (IOException e) {
            System.out.println("Неверный путь к файлу");
        }
        try (Writer writer = new PrintWriter(new FileWriter(file, StandardCharsets.UTF_8))) {
            for (int num : array) {
                try {
                    writer.write(num);
                } catch (IOException e) {
                    System.out.println("Ошибка при записи");
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
    }

    public static int[] readSymbolFromArray(int n) {
        File file = new File("file.txt");
        try (Reader reader = new FileReader(file, StandardCharsets.UTF_8)) {
            int[] array = new int[n];
            for (int i = 0; i < n; i++) {
                array[i] = reader.read();
            }
            return array;
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return new int[0];
    }

    //3. Используя класс RandomAccessFile, прочитайте массив целых чисел, начиная с заданной позиции

    public static int[] readFrom(int numberSymbol) {
        File file = new File("file.txt");
        try (RandomAccessFile fileRead = new RandomAccessFile(file, "r")) {
            fileRead.seek(numberSymbol);

            int[] array = new int[(int) fileRead.length() - numberSymbol];
            for (int i = 0; i < fileRead.length() - numberSymbol; i++) {
                array[i] = fileRead.read();
            }
            return array;
        } catch (FileNotFoundException e) {
            System.out.println("Файл не найден");
        } catch (IOException e) {
            System.out.println("Ошибка ввода-вывода");
        }
        return new int[0];
    }


    public static void main(String[] args) {
//        IntArrayToStream.writeBytes(new int[]{0, 2, 3, 4});
//        System.out.println(Arrays.toString(IntArrayToStream.readBytes(4)));
        IntArrayToStream.writeToSymbol(new int[]{0, 2, 3, 4});
        System.out.println(Arrays.toString(IntArrayToStream.readSymbolFromArray(4)));
        System.out.println(Arrays.toString(readFrom(2)));
    }
}