import java.io.File;
import java.io.FilenameFilter;

/*4. Используя класс File, получите список всех файлов с заданным расширением в заданном
каталоге (поиск в подкаталогах выполнять не надо)*/

public class ExtensionFilter implements FilenameFilter {

    private String extension;

    public ExtensionFilter (String extension) {
        this.extension = extension;
    }

    @Override
    public boolean accept(File dir, String name) {
        return name.endsWith(extension);
    }
}
