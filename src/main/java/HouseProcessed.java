import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;

//6. Напишите сервисный класс с методами, которые сериализуют и десериализуют объект типа
//House в заданный поток средствами Java

public class HouseProcessed {

    public static void serializeHouse (House house, String filename) throws IOException {
        try (ObjectOutput out = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(filename)))){
            out.writeObject(house);
        }
    }

    public static House deserializeHouse (String filename) throws IOException, ClassNotFoundException,
            ClassCastException {
        try (ObjectInput in = new ObjectInputStream(new BufferedInputStream(new FileInputStream(filename)
        ))) {
            return (House) in.readObject();
        }
    }

    //8. Подключите к проекту библиотеку Jackson. Напишите методы сериализации десериализации объектов типа House в строки.
    //Используйте data binding

    public static void serializeJacksonHouse (House house) throws JsonMappingException, JsonGenerationException {
        ObjectMapper mapper = new ObjectMapper();
        try {
            String jsonString = mapper.writeValueAsString(house);
        } catch (JsonMappingException e) {
            e.printStackTrace();
        } catch (JsonGenerationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static House deserializeJacksonHouse (String jsonString) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            House house = mapper.readValue(jsonString, House.class);
            return house;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
